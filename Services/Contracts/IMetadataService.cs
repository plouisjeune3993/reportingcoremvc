﻿using Entities.Report.Metadata;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public interface IMetadataService
    {
        Task<MetaDataSource> GetMetaDataAsync();
    }
}
