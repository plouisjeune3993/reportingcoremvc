﻿using Entities.Report.Metadata;
using Repositories.Contracts;
using Services.Contracts;
using System.Threading.Tasks;

namespace Services
{
    public class MetadataService : IMetadataService
    {
        IMetadataRepository _metadataRepository;

        public MetadataService(IMetadataRepository metadataRepository)
        {
            _metadataRepository = metadataRepository;
        }

        public async Task<MetaDataSource> GetMetaDataAsync() => await _metadataRepository.GetMetaDataAsync();

    }
}
