﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using DevExpress.AspNetCore;
using DevExpress.AspNetCore.Reporting;
using DevExpress.DataAccess.Wizard.Services;
using DevExpress.XtraReports.Web.ClientControls;
using DevExpress.XtraReports.Web.Extensions;
using Infrastructure.Data.API.Contracts;
using Infrastructure.Helpers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using ReportingCore.Storage;
using ReportingCore.Validators;
using Repositories.Contracts;
using Repositories.Implementation;
using Services;
using Services.Contracts;
using System;
using System.IO;

namespace ReportingCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddDevExpressControls();
            services.ConfigureReportingServices(configurator => {
                configurator.ConfigureReportDesigner(designerConfigurator => {
                    designerConfigurator.RegisterDataSourceWizardConfigFileConnectionStringsProvider();
                });
            });
            services.AddSingleton<ReportStorageWebExtension, SimpleReportStorageWebExtension>();
            services.AddMvc()
                    .AddDefaultReportingControllers()
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                    .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.ConfigureReportingServices(configurator => {
                configurator.ConfigureReportDesigner(designerConfigurator => {
                    designerConfigurator.RegisterSqlDataSourceWizardCustomizationService<SimpleSqlDataSourceWizardCustomizationService>();
                });
                configurator.ConfigureReportDesigner(designerConfigurator => {
                    designerConfigurator.EnableCustomSql();
                });
            });
            services.AddSingleton<ICustomQueryValidator, CustomValidator>();
            services.Configure<AppSettingsData>(Configuration.GetSection("AppSettingsData"));
            // Create the container builder.
            var builder = new ContainerBuilder();
            builder.Populate(services);

            //Module registrations
            builder.RegisterType<AppSettingsData>().As<IAppSettingsData>();
            builder.RegisterType<MetadataRepository>().As<IMetadataRepository>();
            builder.RegisterType<MetadataService>().As<IMetadataService>();
            ApplicationContainer = builder.Build();

            // Create the IServiceProvider based on the container.
            return new AutofacServiceProvider(ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerfactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseFileServer(new FileServerOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, "node_modules")),
                RequestPath = "/node_modules"
            });
            app.UseDevExpressControls();

            var reportingLogger = loggerfactory.CreateLogger("Reporting");
            Action<Exception, string> logError = (ex, message) => {
                var errorString = string.Format("[{0}]: Exception occurred. Message: '{1}'. Exception Details:\r\n{2}", DateTime.Now, message, ex);
                reportingLogger.LogError(errorString);
            };
            LoggerService.Initialize(logError);
            ReportStorageWebExtension.RegisterExtensionGlobal(app.ApplicationServices.GetService<ReportStorageWebExtension>());
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
