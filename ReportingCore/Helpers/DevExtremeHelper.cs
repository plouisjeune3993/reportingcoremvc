﻿using DevExpress.XtraPrinting;

namespace ReportingCore.Helpers
{
    public static class DevExtremeHelper
    {
        public static TextAlignment GetTextAlignment(string alignment)
        {
            if (alignment == "topleft") return TextAlignment.TopLeft;
            if (alignment == "topcenter") return TextAlignment.TopCenter;
            if (alignment == "topright") return TextAlignment.TopRight;
            if (alignment == "topjustify") return TextAlignment.TopJustify;
            if (alignment == "bottomleft") return TextAlignment.BottomLeft;
            if (alignment == "bottomright") return TextAlignment.BottomRight;
            if (alignment == "bottomcenter") return TextAlignment.BottomCenter;
            if (alignment == "bottomjustify") return TextAlignment.BottomJustify;
            if (alignment == "middleleft") return TextAlignment.MiddleLeft;
            if (alignment == "middleright") return TextAlignment.MiddleRight;
            if (alignment == "middlecenter" || alignment == "center") return TextAlignment.MiddleCenter;
            return TextAlignment.MiddleCenter;
        }
    }
}
