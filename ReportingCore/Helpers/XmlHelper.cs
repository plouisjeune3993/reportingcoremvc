﻿using System;
using System.Linq;
using System.Xml;

namespace ReportingCore.Helpers
{
    public static class XmlHelper
    {
        public static string RemoveDeclaration(string xml)
        {
            if (string.IsNullOrEmpty(xml)) return "<XtraReportsLayoutSerializer></XtraReportsLayoutSerializer>";
            if(xml.IndexOf(Environment.NewLine) > -1)
            {
                xml = xml.Substring(xml.IndexOf(Environment.NewLine));
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                var declarations = doc.ChildNodes.OfType<XmlNode>()
                                      .Where(x => x.NodeType == XmlNodeType.XmlDeclaration)
                                      .ToList();
                declarations.ForEach(x => doc.RemoveChild(x));
            }

            return xml;
        }
    }
}
