﻿using DevExpress.XtraReports.UI;
using Entities.Report.Template;

namespace ReportingCore.Models
{
    public class ReportTemplateModel
    {
        public Template TemplateModel { get; set; }
        public XtraReport Report { get; set; }
        public string Uuid { get; set; }
    }
}
