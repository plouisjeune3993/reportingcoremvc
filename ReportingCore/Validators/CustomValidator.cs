﻿using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.Wizard.Services;

namespace ReportingCore.Validators
{
    public class CustomValidator : ICustomQueryValidator
    {
        public bool Validate(DataConnectionParametersBase connectionParameters, string sql, ref string message)
        {
            // Add your custom validation logic here.
            // The method should return true if the query is valid and false otherwise.
            return true;
        }
    }
}
