﻿using Microsoft.AspNetCore.Mvc;

namespace ReportingCore.Controllers
{
    public class ConfigurationController : Controller
    {
        public ConfigurationController() { }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("DefaultTemplates")]
        public IActionResult DefaultTemplates() => View("Views/Configuration/Nexus/ListNexusTemplatesView.cshtml");

        [HttpGet]
        [Route("ClientTemplates")]
        public IActionResult ClientTemplates() => View("Views/Configuration/Tenant/ListTenantTemplatesView.cshtml");
    }
}