﻿using DevExpress.DataAccess.Json;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Entities.Report.Metadata;
using Entities.Report.Template;
using Entities.Report.Tenant;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ReportingCore.DBModels;
using ReportingCore.Helpers;
using ReportingCore.Models;
using ReportingCore.Prototype;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;

namespace ReportingCore.Controllers
{
    public class TemplatesController : Controller
    {
        NXS_050Context _context;

        public TemplatesController()
        {
            _context = new NXS_050Context();
        }

        [HttpGet]
        [Route("ReportTemplates")]
        public IActionResult GetTemplates() => View("Views/Template/ReportTemplatesView.cshtml");

        [HttpGet]
        [Route("GetReportTemplateView")]
        public IActionResult EditReportTemplateView(int id = 0)
        {
           ReportTemplateModel reportTemplateModel = new ReportTemplateModel();
           IList<Template> templates = new List<Template>();
           templates.Add(new Template()
           {
               Id = 1014,
               TemplateName = "Pointillé",
               Code = "Pointillé 1",
               Description = "template no. 1",
               LayoutData = "<XtraReportsLayoutSerializer></XtraReportsLayoutSerializer>",
               LastTemplateUpdate = DateTime.Parse("2019-09-04 00:00:01")
           });
           templates.Add(new Template()
           {
                Id = 1016,
                TemplateName = "BluePrint",
                Code = "BluePrint 2",
                Description = "template no. 2",
                LayoutData = "<XtraReportsLayoutSerializer></XtraReportsLayoutSerializer>",
                LastTemplateUpdate = DateTime.Parse("2019-09-04 13:33:01")
           });
           reportTemplateModel.TemplateModel = templates.FirstOrDefault(x => x.Id == id);
           return View("Views/Template/ReportTemplatesEditView.cshtml", reportTemplateModel);
        }

        [HttpGet]
        [Route("ReportDesigner")]
        public IActionResult ShowReportDesigner(string reportModel = null)
        {
            //1. Create new XtraReport
            XtraReport buildReport = new XtraReport();

            //2. Get template instance
            if (reportModel == null) throw new Exception("Report was not found.");
            byte[] data = Convert.FromBase64String(reportModel);
            var uuid = Encoding.UTF8.GetString(data);
            string metadataURL = "http://localhost:8600/tenants/rptModelTenants/"+ uuid; 
            var client = new WebClient();
            var responsedata = client.DownloadString(metadataURL);
            Tenant tenant = JsonConvert.DeserializeObject<Tenant>(responsedata);

            tenant.templateModel = XmlHelper.RemoveDeclaration(tenant.templateModel);
            byte[] templateModelData = Encoding.UTF8.GetBytes(tenant.templateModel);
            buildReport.LoadLayoutFromXml(new MemoryStream(templateModelData));

            //3. Get structured data


            //4. Get byte array from structured data string
            ReportTemplateModel reportTemplateModel = new ReportTemplateModel();
            reportTemplateModel.Report = buildReport;
            reportTemplateModel.Uuid = tenant.uuidRptModelTenant;
            return View("Views/Template/ReportDesignerView.cshtml", reportTemplateModel);
        }

        //[HttpGet]
        //[Route("ReportDesigner")]
        //public IActionResult ShowReportDesigner(int id = 0)
        //{
        //    //1. Create new XtraReport
        //    XtraReport buildReport = new XtraReport();

        //    //2. Get template instance
        //    var report = (from query in _context.Reports
        //                  where query.ReportId == id
        //                  select query).FirstOrDefault();

        //    if (report == null) throw new Exception("Report was not found.");
        //    var layoutData = report.LayoutData;
        //    buildReport.LoadLayoutFromXml(new MemoryStream(layoutData));

        //    //3. Get structured data


        //    //4. Get byte array from structured data string
        //    ReportTemplateModel reportTemplateModel = new ReportTemplateModel();
        //    reportTemplateModel.Report = buildReport;
        //    return View("Views/Template/ReportDesignerView.cshtml", reportTemplateModel);
        //}

        [HttpGet]
        [Route("ReportPreviwer")]
        public IActionResult ShowReportPreviwer(string reportModel = null)
        {
            WebClient client = new WebClient();
            string responsedata, metadataURL;

            //1. Create new XtraReport
            XtraReport buildReport = new XtraReport();

            //2. Get template instance
            if (reportModel == null) throw new Exception("Report was not found.");
            byte[] data = Convert.FromBase64String(reportModel);
            var uuid = Encoding.UTF8.GetString(data);
            metadataURL = "http://localhost:8600/tenants/rptModelTenants/" + uuid;
            responsedata = client.DownloadString(metadataURL);
            Tenant tenant = JsonConvert.DeserializeObject<Tenant>(responsedata);
            byte[] templateModelData = Convert.FromBase64String(tenant.templateModel);
            buildReport.LoadLayoutFromXml(new MemoryStream(templateModelData));

            //3. Get structured data
            //******************************//
            //3.1 Get Metadata information.
            metadataURL = "http://localhost:64770/metadata"; //metadata

            responsedata = client.DownloadString(metadataURL);
            MetaDataSource metaDataColumnElements = JsonConvert.DeserializeObject<MetaDataSource>(responsedata);

            //PageHeaderBand
            PageHeaderBand pageHeaderBand = buildReport.Bands.GetBandByType(typeof(PageHeaderBand)) as PageHeaderBand ?? new PageHeaderBand();
            pageHeaderBand.Name = "PageHeader1";
            pageHeaderBand.HeightF = 24.03f;
            pageHeaderBand.BorderWidth = 1200f;

            //Get XRLabel from PageHeaderBand
            XRLabel xrLabelPageHeader = pageHeaderBand.Controls.OfType<XRLabel>().ToList().FirstOrDefault() as XRLabel;
            xrLabelPageHeader.Visible = false;

            var metadataColumElements = metaDataColumnElements.Columns.Select((value, index) => new { value, index });
            var metadataColumGroupElements = metaDataColumnElements.ColumnGroups.Select((value, index) => new { value, index });
            var metadataColumnAggregateElements = metaDataColumnElements.ColumnAggregates.Select((value, index) => new { value, index });
            var groupElements = metaDataColumnElements.Columns.Where(t2 => !metadataColumGroupElements.Any(t1 => t2.ColumnName.Contains(t1.value.ColumnName)));

            //Header Columns
            foreach (var metadataColumn in groupElements.Select((value, index) => new { value, index }))
            {
                XRLabel xrLabel = new XRLabel();
                xrLabelPageHeader.CopyPropertiesTo<XRLabel, XRLabel>(xrLabel); //Since there are no cloning objects, just copy properties   
                xrLabel.AutoWidth = true;
                xrLabel.Name = $"{metadataColumn.value.ColumnName}{(metadataColumn.index).ToString()}";
                xrLabel.Text = metadataColumn.value.Text;
                xrLabel.TextAlignment = DevExtremeHelper.GetTextAlignment(metadataColumn.value.TextAlignment);
                xrLabel.LocationFloat = new PointFloat(metadataColumn.index * xrLabel.WidthF, xrLabelPageHeader.LocationF.Y);
                xrLabel.Visible = true;
                //xrLabel.SizeF = new System.Drawing.SizeF(75, 50);
                //xrLabel.Padding = new PaddingInfo(5, 5, 5, 5);
                pageHeaderBand.Controls.Add(xrLabel);
            }

            //3.2 Get raw data
            //******************************//
            //Display dynamic data 
            JsonDataSource jsonDataSource = new JsonDataSource();
            jsonDataSource.JsonSource = new UriJsonSource(new Uri("http://localhost:64770/clientdata"));
            jsonDataSource.Fill(); //Retrieve data from the JSON data source
            buildReport.DataSource = jsonDataSource;
            buildReport.DataMember = string.Empty; //this line of code permits to show json data

            DetailBand detailBand = buildReport.Bands.GetBandByType(typeof(DetailBand)) as DetailBand ?? new DetailBand();
            detailBand.Name = "Detail1";
            detailBand.BorderWidth = 1200f;
            detailBand.HeightF = 191.65f;

            //Get XRLabel from DetailBand
            XRLabel xrLabelDetailHeader = detailBand.Controls.OfType<XRLabel>().ToList().FirstOrDefault() as XRLabel;
            xrLabelDetailHeader.Visible = false;

            ReportFooterBand reportFooterBand = buildReport.Bands.GetBandByType(typeof(ReportFooterBand)) as ReportFooterBand ?? new ReportFooterBand();
            reportFooterBand.Name = "reportFooter1";
            reportFooterBand.HeightF = 24.03f;
            reportFooterBand.BorderWidth = 1200f;

            //Get XRLabel from DetailBand
            XRLabel xrLabelReportFooter = reportFooterBand.Controls.OfType<XRLabel>().ToList().FirstOrDefault() as XRLabel;
            xrLabelReportFooter.Visible = false;

            //Using JsonDataSource
            //Group Data
            if (metadataColumGroupElements.Any())
            {
                //GroupHeaderBand ghBand = new GroupHeaderBand { HeightF = 40, BorderWidth = 1000f };
                //buildReport.Bands.Add(ghBand);

                GroupHeaderBand ghBand = buildReport.Bands.GetBandByType(typeof(GroupHeaderBand)) as GroupHeaderBand ?? new GroupHeaderBand();
                //Get XRLabel from GroupHeaderBand
                XRLabel xrLabelGroupHeader = ghBand.Controls.OfType<XRLabel>().ToList().FirstOrDefault() as XRLabel;
                xrLabelGroupHeader.Visible = false;

                foreach (var metadataGroupColumn in metadataColumGroupElements.Select((value, index) => new { value, index }))
                {
                    // Create a group field and assign it to the group header band. 
                    GroupField groupField = new GroupField(metadataGroupColumn.value.value.ColumnName);
                    ghBand.GroupFields.Add(groupField);

                    // Create new labels. 
                    //XRLabel labelGroupInfo = new XRLabel { ForeColor = System.Drawing.Color.Blue, LocationF = new System.Drawing.PointF(0, metadataGroupColumn.index * 30) };
                    //xrLabelGroupHeader.CopyPropertiesTo(labelGroupInfo); //Since there are no cloning objects, just copy properties  
                    //labelGroupInfo.Visible = true;
                    //labelGroupInfo.ExpressionBindings.Add(new ExpressionBinding("BeforePrint", "Text", $"[{metadataGroupColumn.value.value.ColumnLabel}]"));
                    //ghBand.Controls.Add(labelGroupInfo);

                    XRLabel labelGroup = new XRLabel { ForeColor = Color.Blue };
                    xrLabelGroupHeader.CopyPropertiesTo(labelGroup); //Since there are no cloning objects, just copy properties  
                    labelGroup.LocationF = new PointF(metadataGroupColumn.index * labelGroup.WidthF, (metadataGroupColumn.index + 3) * xrLabelGroupHeader.LocationF.Y);
                    labelGroup.Visible = true;
                    labelGroup.ExpressionBindings.Add(new ExpressionBinding("BeforePrint", "Text", $"[{metadataGroupColumn.value.value.ColumnName}]"));
                    ghBand.Controls.Add(labelGroup);
                }

                foreach (var metadataColumn in groupElements.Select((value, index) => new { value, index }))
                {
                    //Create label
                    XRLabel xrLabel1 = new XRLabel();
                    xrLabelDetailHeader.CopyPropertiesTo(xrLabel1); //Since there are no cloning objects, just copy properties   
                    xrLabel1.AutoWidth = true;
                    //xrLabel1.SizeF = new System.Drawing.SizeF(metadataColumn.value.ColumnLength*2, xrLabelPageHeader.SizeF.Height);
                    xrLabel1.TextAlignment = DevExtremeHelper.GetTextAlignment(metadataColumn.value.TextAlignment);
                    xrLabel1.LocationF = new PointF(metadataColumn.index * xrLabel1.WidthF, xrLabelPageHeader.LocationF.Y);
                    xrLabel1.Visible = true;
                    //Show data
                    ExpressionBinding exp1 = new ExpressionBinding("BeforePrint", "Text", $"[{metadataColumn.value.ColumnName}]");
                    xrLabel1.ExpressionBindings.Add(exp1);
                    detailBand.Controls.Add(xrLabel1);
                }

                //Process aggregate
                foreach (var metadataColumnAggregate in metadataColumnAggregateElements.Select((value, index) => new { value, index }))
                {
                    var calcFieldName = string.Empty;
                    CalculatedField calcField = new CalculatedField();
                    calcField.DataSource = buildReport.DataSource;
                    calcField.DataMember = buildReport.DataMember;
                    calcField.FieldType = FieldType.Float;

                    if (metadataColumnAggregate.value.value.Type == "Sum")
                    {
                        calcFieldName = "sumField";
                        calcField.Name = calcFieldName;
                        calcField.DisplayName = "Total Sum Field";
                        calcField.Expression = $"Sum([{metadataColumnAggregate.value.value.ColumnName}])";
                    }
                    if (metadataColumnAggregate.value.value.Type == "Avg")
                    {
                        calcFieldName = "avgField";
                        calcField.Name = calcFieldName;
                        calcField.DisplayName = "Total Avg Field";
                        calcField.Expression = $"Avg([{metadataColumnAggregate.value.value.ColumnName}])";
                    }

                    //Create label to aggregate
                    XRLabel xrAggregateLabelInfo = new XRLabel();
                    xrLabelReportFooter.CopyPropertiesTo(xrAggregateLabelInfo); //Since there are no cloning objects, just copy properties  
                    //xrAggregateLabelInfo.SizeF = new SizeF(60, 45);
                    xrAggregateLabelInfo.TextAlignment = TextAlignment.MiddleCenter;
                    xrAggregateLabelInfo.LocationF = new PointF(0, 30 * metadataColumnAggregate.index);
                    xrAggregateLabelInfo.Padding = new PaddingInfo(5, 5, 5, 5);
                    xrAggregateLabelInfo.Text = metadataColumnAggregate.value.value.ColumnLabel;
                    xrAggregateLabelInfo.Visible = true;

                    XRLabel xrAggregateLabel = new XRLabel();
                    xrLabelReportFooter.CopyPropertiesTo(xrAggregateLabel);
                    //xrAggregateLabel.SizeF = new SizeF(60, 45);
                    xrAggregateLabel.TextAlignment = TextAlignment.MiddleCenter;
                    xrAggregateLabel.LocationF = new PointF(metadataColumnAggregate.index + 5 * 30, 30 * metadataColumnAggregate.index);
                    xrAggregateLabel.Padding = new PaddingInfo(5, 5, 5, 5);
                    xrAggregateLabel.Visible = true;

                    var formatString = $"FormatString('{metadataColumnAggregate.value.value.Format} $', [{calcFieldName}])";
                    ExpressionBinding exp12 = new ExpressionBinding("BeforePrint", "Text", formatString);
                    xrAggregateLabel.ExpressionBindings.Add(exp12);

                    buildReport.CalculatedFields.Add(calcField);
                    reportFooterBand.Controls.Add(xrAggregateLabelInfo);
                    reportFooterBand.Controls.Add(xrAggregateLabel);
                }
            }
            else
            {
                int xCounter = 0;
                foreach (var metadataColumn in metadataColumElements)
                {
                    //Create label
                    XRLabel xrLabel = new XRLabel();
                    xrLabelDetailHeader.CopyPropertiesTo<XRLabel, XRLabel>(xrLabel); //Since there are no cloning objects, just copy properties   
                    xrLabel.SizeF = new SizeF(metadataColumn.value.ColumnLength, xrLabelPageHeader.SizeF.Height);
                    xrLabel.TextAlignment = DevExtremeHelper.GetTextAlignment(metadataColumn.value.TextAlignment);
                    xrLabel.LocationFloat = new PointFloat(metadataColumn.index * 30, xrLabelPageHeader.LocationF.Y);

                    //Show data
                    ExpressionBinding exp = new ExpressionBinding("BeforePrint", "Text", $"[{metadataColumn.value.ColumnName}]");
                    xrLabel.ExpressionBindings.Add(exp);
                    detailBand.Controls.Add(xrLabel);
                    xCounter++;

                }

            }

            buildReport.Bands.Add(detailBand);
            buildReport.Bands.Add(reportFooterBand);

            ReportTemplateModel reportTemplateModel = new ReportTemplateModel();
            reportTemplateModel.Report = buildReport;
            return View("Views/Template/ReportPreviewerView.cshtml", reportTemplateModel);
        }

        //[HttpGet]
        //[Route("ReportPreviwer")]
        //public IActionResult ShowReportPreviwer(int id = 0)
        //{
        //    WebClient client;
        //    string responsedata;

        //    //1. Create new XtraReport
        //    XtraReport buildReport = new XtraReport();

        //    //2. Get template instance
        //    var report = (from query in _context.Reports
        //                  where query.ReportId == id
        //                  select query).FirstOrDefault();

        //    if (report == null) throw new Exception("Report was not found.");
        //    var layoutData = report.LayoutData;
        //    buildReport.LoadLayoutFromXml(new MemoryStream(layoutData));
            
        //    //3. Get structured data
        //    //******************************//
        //    //3.1 Get Metadata information.
        //    string metadataURL = "http://localhost:64770/metadata"; //metadata
        //    client = new WebClient();
        //    responsedata = client.DownloadString(metadataURL);
        //    MetaDataSource metaDataColumnElements = JsonConvert.DeserializeObject<MetaDataSource>(responsedata);

        //    //PageHeaderBand
        //    PageHeaderBand pageHeaderBand = buildReport.Bands.GetBandByType(typeof(PageHeaderBand)) as PageHeaderBand ?? new PageHeaderBand();
        //    pageHeaderBand.Name = "PageHeader1";
        //    pageHeaderBand.HeightF = 24.03f;
        //    pageHeaderBand.BorderWidth = 1200f;

        //    //Get XRLabel from PageHeaderBand
        //    XRLabel xrLabelPageHeader = pageHeaderBand.Controls.OfType<XRLabel>().ToList().FirstOrDefault() as XRLabel;
        //    xrLabelPageHeader.Visible = false;

        //    var metadataColumElements = metaDataColumnElements.Columns.Select((value, index) => new { value, index });
        //    var metadataColumGroupElements = metaDataColumnElements.ColumnGroups.Select((value, index) => new { value, index });
        //    var metadataColumnAggregateElements = metaDataColumnElements.ColumnAggregates.Select((value, index) => new { value, index });
        //    var groupElements = metaDataColumnElements.Columns.Where(t2 => !metadataColumGroupElements.Any(t1 => t2.ColumnName.Contains(t1.value.ColumnName)));

        //    //Header Columns
        //    foreach (var metadataColumn in groupElements.Select((value, index) => new { value, index }))
        //    {
        //        XRLabel xrLabel = new XRLabel();
        //        xrLabelPageHeader.CopyPropertiesTo<XRLabel, XRLabel>(xrLabel); //Since there are no cloning objects, just copy properties   
        //        xrLabel.AutoWidth = true;
        //        xrLabel.Name = $"{metadataColumn.value.ColumnName}{(metadataColumn.index).ToString()}";
        //        xrLabel.Text = metadataColumn.value.Text;
        //        xrLabel.TextAlignment = DevExtremeHelper.GetTextAlignment(metadataColumn.value.TextAlignment);
        //        xrLabel.LocationFloat =  new PointFloat(metadataColumn.index * xrLabel.WidthF, xrLabelPageHeader.LocationF.Y);
        //        xrLabel.Visible = true;
        //        //xrLabel.SizeF = new System.Drawing.SizeF(75, 50);
        //        //xrLabel.Padding = new PaddingInfo(5, 5, 5, 5);
        //        pageHeaderBand.Controls.Add(xrLabel);
        //    }

        //    //3.2 Get raw data
        //    //******************************//
        //    //Display dynamic data 
        //    JsonDataSource jsonDataSource = new JsonDataSource();
        //    jsonDataSource.JsonSource = new UriJsonSource(new Uri("http://localhost:64770/clientdata"));
        //    jsonDataSource.Fill(); //Retrieve data from the JSON data source
        //    buildReport.DataSource = jsonDataSource;
        //    buildReport.DataMember = string.Empty; //this line of code permits to show json data

        //    DetailBand detailBand = buildReport.Bands.GetBandByType(typeof(DetailBand)) as DetailBand ?? new DetailBand();
        //    detailBand.Name = "Detail1";
        //    detailBand.BorderWidth = 1200f;
        //    detailBand.HeightF = 191.65f;

        //    //Get XRLabel from DetailBand
        //    XRLabel xrLabelDetailHeader = detailBand.Controls.OfType<XRLabel>().ToList().FirstOrDefault() as XRLabel;
        //    xrLabelDetailHeader.Visible = false;

        //    ReportFooterBand reportFooterBand = buildReport.Bands.GetBandByType(typeof(ReportFooterBand)) as ReportFooterBand ?? new ReportFooterBand();
        //    reportFooterBand.Name = "reportFooter1";
        //    reportFooterBand.HeightF = 24.03f;
        //    reportFooterBand.BorderWidth = 1200f;

        //    //Get XRLabel from DetailBand
        //    XRLabel xrLabelReportFooter = reportFooterBand.Controls.OfType<XRLabel>().ToList().FirstOrDefault() as XRLabel;
        //    xrLabelReportFooter.Visible = false;

        //    //Using JsonDataSource
        //    //Group Data
        //    if (metadataColumGroupElements.Any())
        //    {
        //        //GroupHeaderBand ghBand = new GroupHeaderBand { HeightF = 40, BorderWidth = 1000f };
        //        //buildReport.Bands.Add(ghBand);

        //        GroupHeaderBand ghBand = buildReport.Bands.GetBandByType(typeof(GroupHeaderBand)) as GroupHeaderBand ?? new GroupHeaderBand();
        //        //Get XRLabel from GroupHeaderBand
        //        XRLabel xrLabelGroupHeader = ghBand.Controls.OfType<XRLabel>().ToList().FirstOrDefault() as XRLabel;
        //        xrLabelGroupHeader.Visible = false;

        //        foreach (var metadataGroupColumn in metadataColumGroupElements.Select((value, index) => new { value, index }))
        //        {
        //            // Create a group field and assign it to the group header band. 
        //            GroupField groupField = new GroupField(metadataGroupColumn.value.value.ColumnName);
        //            ghBand.GroupFields.Add(groupField);

        //            // Create new labels. 
        //            //XRLabel labelGroupInfo = new XRLabel { ForeColor = System.Drawing.Color.Blue, LocationF = new System.Drawing.PointF(0, metadataGroupColumn.index * 30) };
        //            //xrLabelGroupHeader.CopyPropertiesTo(labelGroupInfo); //Since there are no cloning objects, just copy properties  
        //            //labelGroupInfo.Visible = true;
        //            //labelGroupInfo.ExpressionBindings.Add(new ExpressionBinding("BeforePrint", "Text", $"[{metadataGroupColumn.value.value.ColumnLabel}]"));
        //            //ghBand.Controls.Add(labelGroupInfo);

        //            XRLabel labelGroup = new XRLabel { ForeColor = Color.Blue };
        //            xrLabelGroupHeader.CopyPropertiesTo(labelGroup); //Since there are no cloning objects, just copy properties  
        //            labelGroup.LocationF = new PointF(metadataGroupColumn.index * labelGroup.WidthF, (metadataGroupColumn.index+3) * xrLabelGroupHeader.LocationF.Y);
        //            labelGroup.Visible = true;
        //            labelGroup.ExpressionBindings.Add(new ExpressionBinding("BeforePrint", "Text", $"[{metadataGroupColumn.value.value.ColumnName}]"));
        //            ghBand.Controls.Add(labelGroup);
        //        }

        //        foreach (var metadataColumn in groupElements.Select((value, index) => new { value, index }))
        //        {
        //            //Create label
        //            XRLabel xrLabel1 = new XRLabel();
        //            xrLabelDetailHeader.CopyPropertiesTo(xrLabel1); //Since there are no cloning objects, just copy properties   
        //            xrLabel1.AutoWidth = true;
        //            //xrLabel1.SizeF = new System.Drawing.SizeF(metadataColumn.value.ColumnLength*2, xrLabelPageHeader.SizeF.Height);
        //            xrLabel1.TextAlignment = DevExtremeHelper.GetTextAlignment(metadataColumn.value.TextAlignment);
        //            xrLabel1.LocationF = new PointF(metadataColumn.index * xrLabel1.WidthF, xrLabelPageHeader.LocationF.Y);
        //            xrLabel1.Visible = true;
        //            //Show data
        //            ExpressionBinding exp1 = new ExpressionBinding("BeforePrint", "Text", $"[{metadataColumn.value.ColumnName}]");
        //            xrLabel1.ExpressionBindings.Add(exp1);
        //            detailBand.Controls.Add(xrLabel1);
        //        }

        //        //Process aggregate
        //        foreach (var metadataColumnAggregate in metadataColumnAggregateElements.Select((value, index) => new { value, index }))
        //        {
        //            var calcFieldName = string.Empty;
        //            CalculatedField calcField = new CalculatedField();
        //            calcField.DataSource = buildReport.DataSource;
        //            calcField.DataMember = buildReport.DataMember;
        //            calcField.FieldType = FieldType.Float;

        //            if (metadataColumnAggregate.value.value.Type == "Sum")
        //            {
        //                calcFieldName = "sumField";
        //                calcField.Name = calcFieldName;
        //                calcField.DisplayName = "Total Sum Field";
        //                calcField.Expression = $"Sum([{metadataColumnAggregate.value.value.ColumnName}])";
        //            }
        //            if (metadataColumnAggregate.value.value.Type == "Avg")
        //            {
        //                calcFieldName = "avgField";
        //                calcField.Name = calcFieldName;
        //                calcField.DisplayName = "Total Avg Field";
        //                calcField.Expression = $"Avg([{metadataColumnAggregate.value.value.ColumnName}])";
        //            }

        //            //Create label to aggregate
        //            XRLabel xrAggregateLabelInfo = new XRLabel();
        //            xrLabelReportFooter.CopyPropertiesTo(xrAggregateLabelInfo); //Since there are no cloning objects, just copy properties  
        //            //xrAggregateLabelInfo.SizeF = new SizeF(60, 45);
        //            xrAggregateLabelInfo.TextAlignment = TextAlignment.MiddleCenter;
        //            xrAggregateLabelInfo.LocationF = new PointF(0, 30 * metadataColumnAggregate.index);
        //            xrAggregateLabelInfo.Padding = new PaddingInfo(5, 5, 5, 5);
        //            xrAggregateLabelInfo.Text = metadataColumnAggregate.value.value.ColumnLabel;
        //            xrAggregateLabelInfo.Visible = true;

        //            XRLabel xrAggregateLabel = new XRLabel();
        //            xrLabelReportFooter.CopyPropertiesTo(xrAggregateLabel);
        //            //xrAggregateLabel.SizeF = new SizeF(60, 45);
        //            xrAggregateLabel.TextAlignment = TextAlignment.MiddleCenter;
        //            xrAggregateLabel.LocationF = new PointF(metadataColumnAggregate.index+5 * 30, 30 * metadataColumnAggregate.index);
        //            xrAggregateLabel.Padding = new PaddingInfo(5, 5, 5, 5);
        //            xrAggregateLabel.Visible = true;

        //            var formatString = $"FormatString('{metadataColumnAggregate.value.value.Format} $', [{calcFieldName}])";
        //            ExpressionBinding exp12 = new ExpressionBinding("BeforePrint", "Text", formatString);
        //            xrAggregateLabel.ExpressionBindings.Add(exp12);
 
        //            buildReport.CalculatedFields.Add(calcField);
        //            reportFooterBand.Controls.Add(xrAggregateLabelInfo);
        //            reportFooterBand.Controls.Add(xrAggregateLabel);
        //        }
        //    }
        //    else
        //    {
        //        int xCounter = 0;
        //        foreach (var metadataColumn in metadataColumElements)
        //        {
        //            //Create label
        //            XRLabel xrLabel = new XRLabel();
        //            xrLabelDetailHeader.CopyPropertiesTo<XRLabel, XRLabel>(xrLabel); //Since there are no cloning objects, just copy properties   
        //            xrLabel.SizeF = new SizeF(metadataColumn.value.ColumnLength, xrLabelPageHeader.SizeF.Height);
        //            xrLabel.TextAlignment = DevExtremeHelper.GetTextAlignment(metadataColumn.value.TextAlignment);
        //            xrLabel.LocationFloat = new PointFloat(metadataColumn.index * 30, xrLabelPageHeader.LocationF.Y);

        //            //Show data
        //            ExpressionBinding exp = new ExpressionBinding("BeforePrint", "Text", $"[{metadataColumn.value.ColumnName}]");
        //            xrLabel.ExpressionBindings.Add(exp);
        //            detailBand.Controls.Add(xrLabel);
        //            xCounter++;

        //        }

        //    }

        //    buildReport.Bands.Add(detailBand);
        //    buildReport.Bands.Add(reportFooterBand);

        //    ReportTemplateModel reportTemplateModel = new ReportTemplateModel();
        //    reportTemplateModel.Report = buildReport;
        //    return View("Views/Template/ReportPreviewerView.cshtml", reportTemplateModel);
        //}
    }
}