﻿using DevExpress.Compatibility.System.Web;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web.Extensions;
using Entities.Report.Tenant;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ReportingCore.DBModels;
using ReportingCore.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Xml;

namespace ReportingCore.Storage
{
    public class SimpleReportStorageWebExtension : ReportStorageWebExtension
    {
        protected IHostingEnvironment Environment { get; }
        NXS_050Context _context;

        public SimpleReportStorageWebExtension(IHostingEnvironment env)
        {
            Environment = env;
            _context = new NXS_050Context();
        }

        public override bool CanSetData(string url)
        {
            return true;
        }

        public override bool IsValidUrl(string url)
        {
            return true;
        }

        public override byte[] GetData(string id)
        {
            //Get report from db
            XtraReport buildReport = new XtraReport();
            var report = (from query in _context.Reports
                          where query.ReportId == Convert.ToInt32(id)
                          select query).FirstOrDefault();

            if (report == null && (report?.LayoutData ?? null) != null) throw new Exception("Report was not found.");
            var layoutData = report.LayoutData;
            return layoutData;
        }

        public override Dictionary<string, string> GetUrls()
        {
            var dictionaries = new Dictionary<string, string>();
            var reports = (from query in _context.Reports select query).Where(o => o.ReportId == 1014).ToList();
            if (reports == null) return new Dictionary<string, string>();

            foreach (Reports report in reports)
                dictionaries.Add(report.ReportId.ToString(), report.ReportUrl);

            return dictionaries;
        }

        public override void SetData(XtraReport report, string uuid)
        {
            JavaScriptSerializer _serializer = new JavaScriptSerializer();
            List<string> extra = uuid.Split('_').ToList();
            using (var stream = new MemoryStream())
            {
                report.SaveLayoutToXml(stream);

                string metadataURL = "http://localhost:8600/tenants/rptModelTenants/" + extra[0];
                var client = new WebClient();
                var responsedata = client.DownloadString(metadataURL);
                Tenant tenant = JsonConvert.DeserializeObject<Tenant>(responsedata);
                var content = Encoding.UTF8.GetString(stream.ToArray());
                tenant.templateModel = XmlHelper.RemoveDeclaration(content.Replace("\"", "'"));
                
                byte[] dataToSend = Encoding.UTF8.GetBytes(_serializer.Serialize(tenant));
                var putUrl = "http://localhost:8600/tenants/rptModelTenants/" + tenant.uuidRptModelTenant;
                var request = (HttpWebRequest)WebRequest.Create(putUrl);
                request.Method = HttpMethod.Put.Method;
                request.ContentType = "application/json; charset=utf-8";
                request.Timeout = 4000; //ms

                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(dataToSend, 0, dataToSend.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NoContent) return;

            }
        }

        public override string SetNewData(XtraReport report, string uuid)
        {
            //JavaScriptSerializer _serializer = new JavaScriptSerializer();
            //List<string> extra = uuid.Split('_').ToList();
            //if (extra.Count < 1) return string.Empty;
            //using (var stream = new MemoryStream())
            //{
            //    report.SaveLayoutToXml(stream);

            //    string metadataURL = "http://localhost:8600/tenants/rptModelTenants/" + extra[0];
            //    var client = new WebClient();
            //    var responsedata = client.DownloadString(metadataURL);
            //    Tenant tenant = JsonConvert.DeserializeObject<Tenant>(responsedata);
            //    var content = Encoding.UTF8.GetString(stream.ToArray(), 0, stream.ToArray().Length);
            //    tenant.templateModel = XmlHelper.RemoveDeclaration(tenant.templateModel.Replace("\"", "'"));

            //    byte[] dataToSend = Encoding.UTF8.GetBytes(_serializer.Serialize(tenant));
            //    var putUrl = "http://localhost:8600/tenants/rptModelTenants/" + tenant.uuidRptModelTenant;
            //    var request = (HttpWebRequest)WebRequest.Create(putUrl);
            //    request.Method = HttpMethod.Put.Method;
            //    request.ContentType = "application/json; charset=utf-8";
            //    request.Timeout = 4000; //ms

            //    using (var requestStream = request.GetRequestStream())
            //    {
            //        requestStream.Write(dataToSend, 0, dataToSend.Length);
            //    }

            //    var response = (HttpWebResponse)request.GetResponse();
            //    if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.NoContent) return "ok";
            //    return string.Empty;
            //}
            return string.Empty;
        }
    }
}