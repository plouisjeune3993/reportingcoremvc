﻿using DevExpress.DataAccess.Web;
using ReportingCore.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportingCore.Storage
{
    public class SimpleSqlDataSourceWizardCustomizationService : ISqlDataSourceWizardCustomizationService
    {
        public DevExpress.DataAccess.Wizard.Services.ICustomQueryValidator CustomQueryValidator
        {
            get { return new CustomValidator(); }
        }

        public bool IsCustomSqlDisabled
        {
            get { return true; } // Enable custom SQL editing.
        }
    }
}
