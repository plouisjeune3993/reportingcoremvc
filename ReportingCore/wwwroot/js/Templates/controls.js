﻿var saveObj = {};
var listObj = [];
$(function () {
    $("#buttonContainer").dxButton({
        text: "Sauvegarder",
        onClick: function (e) {

        }
    });

    $("#btnAddTemplate").dxButton({
        stylingMode: "contained",
        text: "Ajouter Template",
        type: "normal",
        width: 120,
        onClick: function ()
        {
            //fire the modal
            $('#configurationTenant').modal({ show: true });
        }
    });

    $("#btnAddTemplate").on("click", function () {
        $('#txtcodeRptModelTenant').val("");
        $('#txtdescRptModelTenant').val("");
        $('#chkstatut').prop("checked", true);
        $('#chkisDefaut').prop("checked", false);
        $('#rptTenantId').val("0");
        $('#templateView').hide();

        //fire the modal
        $('#configurationTenant').modal({ show: true });
    });

    $("#btnSaveClientTemplate").on("click", function () {
        var uuid = $("#rptTenantId").val();

        var url = (uuid == "0") ? "http://localhost:8600/tenants/rptModelTenants" : "http://localhost:8600/tenants/rptModelTenants/" + uuid;

        if (uuid == "0") {
            saveObj = {
                uuidTenant: "",
                codeRptModelTenant: $("#txtcodeRptModelTenant").val(),
                descRptModelTenant: $("#txtdescRptModelTenant").val(),
                statut: $("#chkstatut").prop("checked") ? "Actif" : "",
                isDefaut: $("#chkisDefaut").prop("checked"),
                uuidRptModelSys: "",
                templateModel: "",
                sections: [
                    {
                        typeRptSection: "Grid1"
                    }]
            };
        } else {
            saveObj.codeRptModelTenant = $("#txtcodeRptModelTenant").val();
            saveObj.descRptModelTenant = $("#txtdescRptModelTenant").val();
            saveObj.statut = $("#chkstatut").prop("checked") ? "Actif" : "";
            saveObj.isDefaut = $("#chkisDefaut").prop("checked");
        }


        var putTemplate = fetch(url, {
            method: (uuid == "0") == null ? 'POST' : 'PUT',
            body: JSON.stringify(saveObj),
            headers: new Headers({
                'Content-type': 'application/json; charset=UTF-8'
            })
        }).then(response => {
            return response.text();
        }).then(data => {
            //fire the modal
            $("#msgDisplayTenant").html("sauvegardé!");
            $('#msgDisplayTenantModal').modal('show');
            LoadClientTemplates();
        }).catch(function (ex) {
            $("#msgDisplayTenant").html("non-sauvegardé!");
            $('#msgDisplayTenantModal').modal('show');
        });

    });

    var timer;
    $('#txtcodeRptModelTenant, #txtdescRptModelTenant').on('keyup', function (e) {
        var elementId = $(this).attr("id");
        var elementVal = $(this).val();
        var found_templates = $.grep(listObj, function (v) {
            if (elementId == "txtcodeRptModelTenant") return v.codeRptModelTenant.startsWith(elementVal.trim());
            return v.descRptModelTenant.startsWith(elementVal.trim());
        });

        var first = found_templates[0];

        if (elementId == "txtdescRptModelTenant") $('#txtcodeRptModelTenant').val(first === null ? "" : first.codeRptModelTenant);
        if (elementId == "txtcodeRptModelTenant") $('#txtdescRptModelTenant').val(first === null ? "" : first.descRptModelTenant);
        $('#chkstatut').prop("checked", (first === null ? false : (first.statut === "Actif" ? true : false)));
        $('#chkisDefaut').prop("checked", first === null ? false :  first.isDefaut);

        //var url = "http://localhost:8600/tenants/rptModelTenants/";
        //clearTimeout(timer);
        //timer = setTimeout(function () {
        //    fetch(url, { method: "GET" })
        //        .then(response => {
        //            return response.json();
        //        }).then(data => {

        //            // Work with JSON data here
        //            $('#txtcodeRptModelTenant').val(data.codeRptModelTenant);
        //            $('#txtdescRptModelTenant').val(data.descRptModelTenant);
        //            $('#chkstatut').prop("checked", data.statut === "Actif" ? true : false);
        //            $('#chkisDefaut').prop("checked", data.isDefaut);
        //            $('#rptTenantId').val(data.uuidRptModelTenant);
        //            $('#rptModelSysId').val(data.uuidRptModelSys);
        //            $('#templateView').show();
        //            $('#design-button').off();
        //            $('#design-button').on("click", function () { window.open('ReportDesigner?reportModel=' + window.btoa(data.uuidRptModelTenant), "_blank") });
        //            saveObj = data;
        //            //fire the modal
        //            $('#configurationTenant').modal({ show: true });
        //        }).catch(function (ex) {
        //            console.log('parsing failed', ex);
        //        });
        //}, 800);
    });


    $('#configurationNexus').modal({ show: false });
});