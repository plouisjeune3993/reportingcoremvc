﻿
var listClientTemplates = [];
function LoadClientTemplates() {

    $(function () {

        var client_templates = new DevExpress.data.CustomStore({
            key: "uuidRptModelSys",
            byKey: function (key) {
                return listClientTemplates[key];
            },
            load: function (loadOptions) {
                var d = $.Deferred();
                $.when($.getJSON("http://localhost:8600/tenants/rptModelTenants")
                    .done(function (result) {
                        console.log(result);
                        listClientTemplates = listObj = result;
                        d.resolve(listClientTemplates);
                    }));
                return d.promise();
            },
            cacheRawData: false
        });

        $("#ddlClientTemplates").dxDataGrid({
            allowColumnResizing: true,
            columnAutoWidth: true,
            width: "100%",
            cacheEnabled: false,
            dataSource: { store: client_templates },
            showBorders: true,
            sorting: {
                mode: "multiple"
            },
            rowTemplate: function (rowElement, rowInfo) {
                if (rowInfo.rowType === "data") {
                    var markup;
                    markup =
                        "<tr class='dx-row main-row draggableRow' keyValue=" + rowInfo.rowIndex + " uid=" + rowInfo.data.uuidRptModelTenant + ">" +
                        "<td class='text-left'>" + rowInfo.data.codeRptModelTenant + "</td>" +
                        "<td class='text-left'>" + rowInfo.data.descRptModelTenant + "</td>" +
                        "<td class='text-left'>" + rowInfo.data.statut + "</td>" +
                        "<td class='text-left'>" + rowInfo.data.isDefaut + "</td>" +
                        "<td class='text-left'><button onclick='loadClientTemplatesById(\"" + rowInfo.data.uuidRptModelTenant + "\")'>Modifier</button></td>" +
                        "</tr>";
                    rowElement.append(markup);
                }
            },
            columns: [
                {
                    caption: "Code",
                    dataField: "codeRptModelTenant",
                    allowSorting: true,
                    visible: true
                },
                {
                    caption: "Description",
                    dataField: "descRptModelTenant",
                    allowSorting: true,
                    visible: true
                },
                {
                    caption: "Statut",
                    dataField: "statut",
                    visible: true
                },
                {
                    caption: "Is Default",
                    dataField: "isDefaut",
                    visible: true
                },
                {
                    caption: "",
                    visible: true
                }
            ]
        });
    });
}

function loadClientTemplatesById(uuid) {
    $(function () {
        var url = "http://localhost:8600/tenants/rptModelTenants/" + uuid;
        //call the API we just made
        fetch(url, { method: "GET" })
            .then(response => {
                return response.json();
            }).then(data => {
                // Work with JSON data here

                $('#txtcodeRptModelTenant').val(data.codeRptModelTenant);
                $('#txtdescRptModelTenant').val(data.descRptModelTenant);
                $('#chkstatut').prop("checked", data.statut === "Actif" ? true : false);
                $('#chkisDefaut').prop("checked", data.isDefaut);
                $('#rptTenantId').val(data.uuidRptModelTenant);
                $('#rptModelSysId').val(data.uuidRptModelSys);
                $('#templateView').show();
                $('#design-button').off();
                $('#design-button').on("click", function () { window.open('ReportDesigner?reportModel=' + window.btoa(data.uuidRptModelTenant), "_blank") });
                saveObj = data;
                //fire the modal
                $('#configurationTenant').modal({ show: true });
            }).catch(function (ex) {
                console.log('parsing failed', ex);
            });
    });
}