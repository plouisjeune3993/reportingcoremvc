﻿
function LoadReportTemplates(templatesData) {
    $(function () {
        $("#ddlReportTemplates").dxDataGrid({
            dataSource: templatesData,
            showBorders: true,
            columnAutoWidth: true,
            sorting: {
                mode: "multiple"
            },
            rowTemplate: function (rowElement, rowInfo) {
                if (rowInfo.rowType === "data") {
                    var markup;
                    markup = "<tbody class='connected-sortable'>" +
                        "<tr class='dx-row main-row draggableRow' keyValue=" + rowInfo.rowIndex + ">" +
                        "<td class='text-left'>" + rowInfo.data.TemplateName + " - " + rowInfo.data.Code + "</td>" +
                        "<td class='text-left'>" + rowInfo.data.Description + "</td>" +
                        "<td class='text-left'>" + rowInfo.data.LastTemplateUpdate + "</td>" +
                        "<td class='text-left'><a href='GetReportTemplateView?id=" + rowInfo.data.Id + "' target='_parent'><button class='btn'><i class='fa fa-pencil-square-o'></i></button></a>&nbsp;"+
                        "<a href='ReportDesigner' target='_parent'><button class='btn'><i class='fa fa-toggle-on'></i></button></a></td>" +
                        "</tr></tbody>";

                    rowElement.append(markup);
                }
            },
            columns: [
                {
                    caption: "TemplateName",
                    allowSorting: true,
                    visible: true
                },
                {
                    caption: "Description",
                    allowSorting: true,
                    visible: true
                },
                {
                    caption: "LastTemplateUpdate",
                    allowSorting: true,
                    visible: true
                },
                {
                    caption: "",
                    visible: true
                }
            ]
        });
    });
}
