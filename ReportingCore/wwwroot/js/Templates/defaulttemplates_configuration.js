﻿
var listTemplates = [];
function LoadDefaultTemplates() {

    $(function () {

        var default_templates = new DevExpress.data.CustomStore({
            key: "uuidRptModelSys",
            byKey: function (key) {
                return listTemplates[key];
            },
            load: function (loadOptions) {
                var d = $.Deferred();
                $.when($.getJSON("http://localhost:8600/nexus/rptModelSyses")
                    .done(function (result) {
                        console.log(result);
                        listTemplates = result;
                        d.resolve(listTemplates);
                    }));
                return d.promise();
            },
            cacheRawData: false
        });

        $("#ddlDefaultTemplates").dxDataGrid({
            allowColumnResizing: true,
            columnAutoWidth: true,
            width: "100%",
            cacheEnabled: false,
            dataSource: { store: default_templates },
            showBorders: true,
            sorting: {
                mode: "multiple"
            },
            rowTemplate: function (rowElement, rowInfo) {
                if (rowInfo.rowType === "data") {
                    var markup;
                    markup =
                        "<tr class='dx-row main-row draggableRow' keyValue=" + rowInfo.rowIndex + " uid=" + rowInfo.data.uuidRptModelSys + ">" +
                        "<td class='text-left'>" + rowInfo.data.codeRptModelSys + "</td>" +
                        "<td class='text-left'>" + rowInfo.data.descRptModelSys + "</td>" +
                        "<td class='text-left'>" + rowInfo.data.codeStatut + "</td>" +
                        "<td class='text-left'>" + rowInfo.data.statut + "</td>" +
                        "<td class='text-left'><button v-on:click='loadDefaultTemplatesById(\"" + rowInfo.data.uuidRptModelSys + "\")'>Modifier</button></td>" +
                        "</tr>";
                    rowElement.append(markup);
                }
            },
            columns: [
                {
                    caption: "Code",
                    allowSorting: true,
                    visible: true
                },
                {
                    caption: "Description",
                    allowSorting: true,
                    visible: true
                },
                {
                    caption: "Code Statut",
                    visible: true
                },
                {
                    caption: "Statut",
                    visible: true
                },
                {
                    caption: "",
                    visible: true
                }
            ]
        });
    });
}