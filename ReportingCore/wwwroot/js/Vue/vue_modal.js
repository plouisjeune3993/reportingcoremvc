﻿document.addEventListener("DOMContentLoaded", function (event) {
    new Vue({
        el: '#app',
        data: {
            defaultTemplates: []
        },
        methods: {
            loadDefaultTemplatesById: function(uuid) {
                var self = this;
                var url = "http://localhost:8600/nexus/rptModelSyses/" + uuid;
                fetch(url, { method: "GET" })
                    .then(response => response.json())
                    .then(function (return_data) {
                        self.defaultTemplates = return_data;
                        console.log("get result: " + return_data);
                    });
            }
        }
    });


});