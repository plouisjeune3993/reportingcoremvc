﻿function reportSaving(s, e) {
    s.ReportStorageSetData(e.Report.serialize(), $("#hdnUuid").val() + "_" + e.Url)
     .done(function (result) {
        console.log("success!");
         $("#msgDisplay").html("Rapport sauvegardé!");
         $('#exampleModal').modal('show');
         $(".dx-toast-wrapper").remove();
    }).catch(function() {
        console.log("error");
        $("#msgDisplay").html("Rapport non-sauvegardé!");
        $('#exampleModal').modal('show');
        $(".dx-toast-wrapper").remove();
    });
}