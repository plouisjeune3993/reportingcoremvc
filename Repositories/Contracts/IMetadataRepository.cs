﻿using Entities.Report.Metadata;
using System.Threading.Tasks;

namespace Repositories.Contracts
{
    public interface IMetadataRepository
    {
        Task<MetaDataSource> GetMetaDataAsync();
    }
}
