﻿using Entities.Report.Metadata;
using Infrastructure.Data.API.Contracts;
using Infrastructure.Data.API.Implementation;
using Repositories.Contracts;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Repositories.Implementation
{
    public class MetadataRepository : IMetadataRepository
    {
        private MetadataAPI _metadataAPI;

        public MetadataRepository(IAppSettingsData appSettingsData) { _metadataAPI = new MetadataAPI(appSettingsData); }

        public async Task<MetaDataSource> GetMetaDataAsync()
        {
            try
            {
                return await _metadataAPI.GetAsync();
            }
            catch (WebException webex)
            {
                WebResponse errResp = webex.Response;
                using (Stream respStream = errResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(respStream);
                    string text = reader.ReadToEnd();
                }
                return await Task.FromResult(new MetaDataSource());
            }
            catch
            {
                return await Task.FromResult(new MetaDataSource());
            }
        }
    }
}
