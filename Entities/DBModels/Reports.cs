﻿using System;
using System.Collections.Generic;

namespace ReportingCore.DBModels
{
    public partial class Reports
    {
        public int ReportId { get; set; }
        public string DisplayName { get; set; }
        public byte[] LayoutData { get; set; }
        public string ReportUrl { get; set; }
    }
}
