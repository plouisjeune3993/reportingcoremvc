﻿using Microsoft.EntityFrameworkCore;

namespace ReportingCore.DBModels
{
    public partial class NXS_050Context : DbContext
    {
        public NXS_050Context()
        {
        }

        public NXS_050Context(DbContextOptions<NXS_050Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Reports> Reports { get; set; }
        public virtual DbSet<SqlQuery> SqlQuery { get; set; }
        public virtual DbSet<SqlQueryColumn> SqlQueryColumn { get; set; }
        public virtual DbSet<SqlQueryParam> SqlQueryParam { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=PRALOU-W10\\DSH001;Database=NXS_050;Trusted_Connection=True;");
                optionsBuilder.EnableSensitiveDataLogging();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Reports>(entity =>
            {
                entity.HasKey(e => e.ReportId);

                entity.ToTable("Reports", "Quest");

                entity.Property(e => e.ReportId).HasColumnName("ReportID");

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.LayoutData).IsRequired();

                entity.Property(e => e.ReportUrl)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SqlQuery>(entity =>
            {
                entity.HasKey(e => e.NidSqlQuery);

                entity.ToTable("SqlQuery", "Quest");

                entity.HasIndex(e => e.CodeSqlQuery)
                    .HasName("AK_SqlQuery_Code")
                    .IsUnique();

                entity.Property(e => e.NidSqlQuery)
                    .HasColumnName("NIdSqlQuery")
                    .ValueGeneratedNever();

                entity.Property(e => e.ApiDtoTypeName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApiModelTypeName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApiRelativeNamespace)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApiRelativePath)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CodeSqlQuery)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.CommentSqlQuery).IsUnicode(false);

                entity.Property(e => e.DescSqlQuery)
                    .IsRequired()
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.ScriptSqlQuery).IsUnicode(false);

                entity.Property(e => e.StoredProcName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SqlQueryColumn>(entity =>
            {
                entity.HasKey(e => e.NidSqlQueryColumn)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("SqlQueryColumn", "Quest");

                entity.HasIndex(e => new { e.NidSqlQuery, e.ColumnIndex })
                    .HasName("AK_SqlQueryColumn_Index")
                    .IsUnique();

                entity.HasIndex(e => new { e.NidSqlQuery, e.ColumnName })
                    .HasName("AK_SqlQueryColumn_Name")
                    .IsUnique()
                    .ForSqlServerIsClustered();

                entity.Property(e => e.NidSqlQueryColumn)
                    .HasColumnName("NIdSqlQueryColumn")
                    .ValueGeneratedNever();

                entity.Property(e => e.ApiComputedExpression).IsUnicode(false);

                entity.Property(e => e.ApiTypeName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CodeDomain)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ColumnName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ForeignEntityTypeName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NidSqlQuery).HasColumnName("NIdSqlQuery");

                entity.Property(e => e.ShapedUuidFieldName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SqlTypeName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UserTypeName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SqlQueryParam>(entity =>
            {
                entity.HasKey(e => e.NidSqlQueryParam)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("SqlQueryParam", "Quest");

                entity.HasIndex(e => new { e.NidSqlQuery, e.ParamName })
                    .HasName("AK_SqlQueryParam")
                    .IsUnique()
                    .ForSqlServerIsClustered();

                entity.Property(e => e.NidSqlQueryParam)
                    .HasColumnName("NIdSqlQueryParam")
                    .ValueGeneratedNever();

                entity.Property(e => e.ApiTypeName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NidSqlQuery).HasColumnName("NIdSqlQuery");

                entity.Property(e => e.ParamName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShapedUuidParamName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SqlTypeName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
        }
    }
}
