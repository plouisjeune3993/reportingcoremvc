﻿using System;
using System.Collections.Generic;

namespace ReportingCore.DBModels
{
    public partial class SqlQuery
    {
        public int NidSqlQuery { get; set; }
        public string CodeSqlQuery { get; set; }
        public string DescSqlQuery { get; set; }
        public int TypeApiSource { get; set; }
        public int TypeSqlQuery { get; set; }
        public string ScriptSqlQuery { get; set; }
        public string StoredProcName { get; set; }
        public string CommentSqlQuery { get; set; }
        public string ApiRelativeNamespace { get; set; }
        public string ApiRelativePath { get; set; }
        public string ApiModelTypeName { get; set; }
        public string ApiDtoTypeName { get; set; }
    }
}
