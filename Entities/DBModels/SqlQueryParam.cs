﻿using System;
using System.Collections.Generic;

namespace ReportingCore.DBModels
{
    public partial class SqlQueryParam
    {
        public int NidSqlQueryParam { get; set; }
        public int NidSqlQuery { get; set; }
        public string ParamName { get; set; }
        public int ParamIndex { get; set; }
        public int TypeSqlType { get; set; }
        public string ApiTypeName { get; set; }
        public string SqlTypeName { get; set; }
        public bool IsSqlParam { get; set; }
        public bool IsOptional { get; set; }
        public bool IsShapedUuid { get; set; }
        public string ShapedUuidParamName { get; set; }
    }
}
