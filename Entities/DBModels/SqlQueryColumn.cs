﻿using System;
using System.Collections.Generic;

namespace ReportingCore.DBModels
{
    public partial class SqlQueryColumn
    {
        public int NidSqlQueryColumn { get; set; }
        public int NidSqlQuery { get; set; }
        public string ColumnName { get; set; }
        public int ColumnIndex { get; set; }
        public bool IsEnabled { get; set; }
        public string CodeDomain { get; set; }
        public string UserTypeName { get; set; }
        public int TypeSqlType { get; set; }
        public string SqlTypeName { get; set; }
        public string ApiTypeName { get; set; }
        public bool IsNullable { get; set; }
        public bool IsApiComputed { get; set; }
        public string ApiComputedExpression { get; set; }
        public bool IsPrimaryKey { get; set; }
        public bool IsForeignKey { get; set; }
        public string ForeignEntityTypeName { get; set; }
        public bool IsShapedUuid { get; set; }
        public string ShapedUuidFieldName { get; set; }
    }
}
