﻿namespace Entities.Report.Metadata
{
    public class ColumnAggregate
    {
        public string Type { get; set; }
        public string ColumnName { get; set; }
        public string ColumnLabel { get; set; }
        public string Format { get; set; }
        public string Formula { get; set; }
    }
}
