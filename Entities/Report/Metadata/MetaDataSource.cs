﻿using Entities.Contracts.Marker;
using System.Collections.Generic;

namespace Entities.Report.Metadata
{
    public class MetaDataSource
    {
        public List<ColumnMetadata> Columns { get; set; } = new List<ColumnMetadata>();
        public List<ColumnGroup> ColumnGroups { get; set; } = new List<ColumnGroup>();
        public List<ColumnAggregate> ColumnAggregates { get; set; } = new List<ColumnAggregate>();
    }
}
