﻿namespace Entities.Report.Metadata
{
    public class ColumnGroup
    {
        public string ColumnName { get; set; }
        public string ColumnLabel { get; set; }
    }
}
