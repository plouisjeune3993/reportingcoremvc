﻿namespace Entities.Report.Metadata
{
    public class ColumnMetadata
    {
        public string ColumnName { get; set; }
        public string Text { get; set; }
        public int ColumnLength { get; set; }
        public string TextAlignment { get; set; }
    }
}
