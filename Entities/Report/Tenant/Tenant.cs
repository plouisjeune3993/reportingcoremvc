﻿using System.Collections.Generic;

namespace Entities.Report.Tenant
{
    public class Tenant
    {
        public string uuidRptModelTenant { get; set; }
        public string uuidTenant { get; set; }

        public string codeRptModelTenant { get; set; }
        public string descRptModelTenant { get; set; }

        public string statut { get; set; }
        public string uuidRptModelSys { get; set; }

        public bool? isDefaut { get; set; }
        public string templateModel { get; set; }

        public List<Section> sections { get; set; }
    }

}
