﻿using System;

namespace Entities.Report.Template
{
    public class Template
    {
        public int Id { get; set; }
        public string TemplateName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string LayoutData { get; set; }
        public bool IsUsed { get; set; }
        public DateTime? LastTemplateUpdate { get; set; }
    }
}
