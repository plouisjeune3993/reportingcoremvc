﻿namespace Entities.Report.Template
{
    public class TemplateAttribute
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Attribute { get; set; }
        public string Color { get; set; }
        public bool IsUsed { get; set; }
    }
}
