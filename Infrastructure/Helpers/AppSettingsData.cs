﻿using Entities;
using Infrastructure.Data.API.Contracts;
using Microsoft.Extensions.Options;

namespace Infrastructure.Helpers
{
    public class AppSettingsData : IAppSettingsData
    {
        private AppSettings AppSettings { get; set; }

        public AppSettingsData(IOptions<AppSettings> settings)
        {
            AppSettings = settings.Value;
        }

        public string GetBaseApiUrl() => AppSettings.ApiUrlConnection; 
    }
}
