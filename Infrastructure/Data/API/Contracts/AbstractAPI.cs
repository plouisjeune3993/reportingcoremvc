﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Infrastructure.Data.API.Contracts
{
    public abstract class AbstractAPI
    {
        protected virtual string GetBaseApiUrl { get; set; }

        public AbstractAPI()
        {
        }

        protected async Task<T> ExecuteApiCall<T>(HttpWebRequest request, byte[] byteData = null) 
        {
            try
            {
                Stream stream = await request.GetRequestStreamAsync();// Gets awaited here indefinitely till the first que of 2 completes their GetResponseAsync() call below

                if (byteData != null) stream.Write(byteData, 0, byteData.Length);
                await stream.FlushAsync();
                stream.Dispose();
                stream = null;

                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                stream = response.GetResponseStream();
                using (StreamReader reader = new StreamReader(stream))
                {
                    var jsonValue = await reader.ReadToEndAsync();
                    var apiResult = JsonConvert.DeserializeObject<T>(jsonValue);
                    return apiResult;
                }
            }
            catch (Exception ex)
            {
                return default(T);
            }

        }
    }
}
