﻿namespace Infrastructure.Data.API.Contracts
{
    public interface IAppSettingsData
    {
        string GetBaseApiUrl();
    }
}
