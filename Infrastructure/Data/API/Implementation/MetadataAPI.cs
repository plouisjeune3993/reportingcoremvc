﻿using Entities.Report.Metadata;
using Infrastructure.Data.API.Contracts;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Infrastructure.Data.API.Implementation
{
    public class MetadataAPI : AbstractAPI
    {
        private readonly IAppSettingsData _appSettingsData;
        protected override string GetBaseApiUrl => $"{_appSettingsData.GetBaseApiUrl()}/metadata";

        public MetadataAPI(IAppSettingsData appSettingsData)
        {
            _appSettingsData = appSettingsData;
        }

        public async Task<MetaDataSource> GetAsync()
        {
            string newUrl = string.Empty;
            newUrl = $"{ GetBaseApiUrl }";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(newUrl);
            request.Method = HttpMethod.Get.Method;
            //request.Headers.Add(QueryStringParams.Key, _mainKey.ToString());
            request.Accept = "application/json";
            return await ExecuteApiCall<MetaDataSource>(request);
        }
    }
}
